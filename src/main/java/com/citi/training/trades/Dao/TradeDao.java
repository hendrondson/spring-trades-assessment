package com.citi.training.trades.Dao;

import java.util.List;

import com.citi.training.trades.model.Trade;

public interface TradeDao {

	List<Trade> findAllTrades();

	Trade findById(int id);

	Trade createTrade(Trade trade);

	void deleteById(int id);
}
