package com.citi.training.trades;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class SpringTradesAssessmentApplication {

	public static void main(String[] args) {
		SpringApplication.run(SpringTradesAssessmentApplication.class, args);
	}

}
