package com.citi.training.trades.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.citi.training.trades.Dao.TradeDao;
import com.citi.training.trades.model.Trade;

@Component
public class TradeService {

	@Autowired
	TradeDao tradeDao;

	public List<Trade> findAllTrades() {
		return tradeDao.findAllTrades();
	}

	public Trade findById(int id) {
		return tradeDao.findById(id);
	}

	public Trade createTrade(Trade trade) {
		return tradeDao.createTrade(trade);
	}

	public void deleteById(int id) {
		tradeDao.deleteById(id);
	}
}
