package com.citi.training.trades.dao.tests;

import static org.junit.Assert.assertEquals;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.transaction.annotation.Transactional;

import com.citi.training.trades.Dao.MySqlTradeDao;
import com.citi.training.trades.model.Trade;

@RunWith(SpringRunner.class)
@SpringBootTest
@ActiveProfiles("h2")
public class MySqlTradeDaoTests {

	@Autowired
	MySqlTradeDao mysqlTradeDao;

	@Test
	@Transactional
	public void test_createAndFindAll() {
		mysqlTradeDao.createTrade(new Trade(-1, "GOOGL", 10.0, 25));

		assertEquals(1, mysqlTradeDao.findAllTrades().size());
	}

}